package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.database.DatabaseConnection;
import model.entities.Department;
import model.entities.DepartmentManager;
import model.entities.Employee;
import model.entities.Gender;
import model.entities.Salary;
import model.entities.Title;

public class EmployeeDAO implements IEmployeeDAO {

	@Override
	public Employee getByEmp_no(int emp_no) throws Exception {
		// TODO Auto-generated method stub
		String query = "SELECT * FROM employees WHERE emp_no=?";
		PreparedStatement pe = DatabaseConnection.getInstance().getConnection().prepareStatement(query);
		pe.setInt(1, emp_no);
		ResultSet rs = pe.executeQuery();
		Employee e = null;
		while (rs.next()) {
			e = new Employee();
			e.setEmp_no(emp_no);
			e.setFirst_name(rs.getString("first_name"));
			e.setLast_name(rs.getString("last_name"));
			e.setGender(Gender.getGender(rs.getString("gender")));
			e.setHire_date(new Date(rs.getDate("hire_date").getTime()));
			e.setBirth_date(new Date(rs.getDate("birth_date").getTime()));
		}
		return e;
	}

	@Override
	public void save(Employee e) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Employee e) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Employee> getAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Employee> getByNameOrLastName(String text) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Salary> getEmployeeSalaries(int employeeId) throws Exception {
		String query="SELECT * FROM salaries WHERE"
				+ " emp_no=?";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		List<Salary> salaries = new ArrayList<Salary>();
		while(rs.next()){
			Salary s = new Salary();
			s.setSalary(rs.getInt("salary"));
			s.setFrom_date(new Date(rs.getDate("from_date").getTime()));
			s.setTo_date(new Date(rs.getDate("to_date").getTime()));
			salaries.add(s);
		}
		return salaries;
	}

	@Override
	public List<Salary> getCurrentSalaries(int employeeId) throws Exception {
		String query="SELECT * FROM salaries WHERE"
				+ " emp_no=? AND"
				+ " NOW() BETWEEN from_date AND to_date";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		List<Salary> salaries = new ArrayList<Salary>();
		while(rs.next()){
			Salary s = new Salary();
			s.setSalary(rs.getInt("salary"));
			s.setFrom_date(new Date(rs.getDate("from_date").getTime()));
			s.setTo_date(new Date(rs.getDate("to_date").getTime()));
			salaries.add(s);
		}
		return salaries;
	}
	
	@Override
	public List<Salary> getCurrentSalariesWithEmployee(int employeeId) throws Exception {
		String query="SELECT * FROM salaries WHERE"
				+ " emp_no=? AND"
				+ " NOW() BETWEEN from_date AND to_date";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		List<Salary> salaries = new ArrayList<Salary>();
		while(rs.next()){
			Salary s = new Salary();
			s.setEmployee(this.getByEmp_no(employeeId));
			s.setSalary(rs.getInt("salary"));
			s.setFrom_date(new Date(rs.getDate("from_date").getTime()));
			s.setTo_date(new Date(rs.getDate("to_date").getTime()));
			salaries.add(s);
		}
		return salaries;
	}

	@Override
	public List<Title> getEmployeeTitles(int employeeId) throws Exception {
		String query="SELECT * FROM titles WHERE"
				+ " titles.emp_no=?";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		List<Title> titles = new ArrayList<Title>();
		while(rs.next()){
			Title t = new Title();
			t.setTitle(rs.getString("title"));
			t.setfrom_date(new Date(rs.getDate("from_date").getTime()));
			t.setto_date(new Date(rs.getDate("to_date").getTime()));
			titles.add(t);
		}
		return titles;
	}

	@Override
	public List<Title> getCurrentTitles(int employeeId) throws Exception {
		String query="SELECT * FROM titles WHERE"
				+ " titles.emp_no=? AND"
				+ " NOW() BETWEEN from_date AND to_date";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		List<Title> titles = new ArrayList<Title>();
		while(rs.next()){
			Title t = new Title();
			t.setTitle(rs.getString("title"));
			t.setfrom_date(new Date(rs.getDate("from_date").getTime()));
			t.setto_date(new Date(rs.getDate("to_date").getTime()));
			titles.add(t);
		}
		return titles;
	}

	@Override
	public List<Department> getEmployeeDepartments(int employeeId) throws Exception {
		String query = "SELECT * FROM departments INNER JOIN dept_emp" 
				+ " ON dept_emp.emp_no=?";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		List<Department> departments = new ArrayList<Department>();
		while (rs.next()) {
			Department d = new Department();
			d.setDept_no(rs.getString("dept_no"));
			d.setDept_name("dept_name");
			departments.add(d);
		}
		return departments;
	}

	@Override
	public List<Department> getCurrentDepartments(int employeeId) throws Exception {
		String query = "SELECT * FROM departments INNER JOIN dept_emp" 
				+ " ON dept_emp.dept_no=departments.dept_no WHERE"
				+ " NOW() BETWEEN dept_emp.from_date AND" 
				+ " dept_emp.to_date AND dept_emp.emp_no=?";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		List<Department> departments = new ArrayList<Department>();
		while (rs.next()) {
			Department d = new Department();
			d.setDept_no(rs.getString("dept_no"));
			d.setDept_name(rs.getString("dept_name"));
			departments.add(d);
		}
		return departments;
	}

	@Override
	public List<Employee> getEmployeeManagers(int employeeId) throws Exception {
		// TODO Auto-generated method stub
		return null;

	}

	@Override
	public List<Employee> getCurrentManagers(int employeeId) throws Exception {
		String query = "SELECT * FROM dept_emp WHERE emp_no=?" 
				+ " AND NOW() BETWEEN from_date AND to_date ";
		PreparedStatement ps = DatabaseConnection.getInstance()
				.getConnection().prepareStatement(query);
		ps.setInt(1, employeeId);
		ResultSet rs = ps.executeQuery();
		ArrayList<Employee> managers = new ArrayList<Employee>();
		while (rs.next()) {
			String dept_no = rs.getString("dept_no");

			String query2 = "SELECT * FROM dept_manager WHERE dept_no=?" + "AND NOW() BETWEEN from_date AND to_date ";

			PreparedStatement ps2 = DatabaseConnection.getInstance().getConnection().prepareStatement(query2);
			ps2.setString(1, dept_no);
			ResultSet rs2 = ps2.executeQuery();

			while (rs2.next()) {
				Employee e = this.getByEmp_no(rs2.getInt("emp_no"));
				managers.add(e);
			}

		}
		return managers;
	}
	
	public static void main (String args[]){
		// Probar DAO
		EmployeeDAO dao = new EmployeeDAO();
		try {
			List<Department> departments = dao
					.getCurrentDepartments(10004);
			for(Department d:departments){
				System.out.println(d.getDept_name() + " " + d.getDept_no());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Employee e = dao.getByEmp_no(10004);
			System.out.println(e.getEmp_no() + " " +
								e.getFirst_name() +" " +
								e.getLast_name() + " " +
								e.getGender());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			List<Salary> salaries = dao.getCurrentSalariesWithEmployee(10004);
			for(Salary s:salaries){
				System.out.println(s.getEmployee().getFirst_name() + " " +
									s.getSalary());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			List<Salary> salaries = dao.getEmployeeSalaries(10004);
			for(Salary s:salaries){
				System.out.println(s.getSalary() +" " + s.getFrom_date() 
									+ " " + s.getTo_date());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			List<Title> titles = dao.getEmployeeTitles(10004);
			for(Title t: titles){
				System.out.println(t.getTitle() + " "
									+ t.getfrom_date() +" "
									+ t.getto_date());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	

}
