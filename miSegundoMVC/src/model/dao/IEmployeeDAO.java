package model.dao;

import java.util.List;

import model.entities.Department;
import model.entities.DepartmentManager;
import model.entities.Employee;
import model.entities.Salary;
import model.entities.Title;

public interface IEmployeeDAO {

	// obtiene un empleado a trav�s de su Id
	public Employee getByEmp_no(int emp_no) throws Exception;

	// guarda a un nuevo empleado
	public void save(Employee e) throws Exception;

	// actualiza a un empleado
	public void update(Employee e) throws Exception;

	// buscar todos los empleados
	public List<Employee> getAll() throws Exception;

	// buscar empleados por nombre o apellido
	public List<Employee> getByNameOrLastName(String text) throws Exception;

	// buscar los salarios de un empleado
	public List<Salary> getEmployeeSalaries(int employeeId) throws Exception;

	// buscar el salario Actual
	public List<Salary> getCurrentSalaries(int employeeId) throws Exception;
	
	// buscar el salario Actual cargando empleado
	public List<Salary> getCurrentSalariesWithEmployee(int employeeId) throws Exception;

	// buscar los puestos de un empleado
	public List<Title> getEmployeeTitles(int employeeId) throws Exception;

	// buscar el puesto actual de un empleado
	public List<Title> getCurrentTitles(int employeeId) throws Exception;

	// buscar los departametos de un empleado
	public List<Department> getEmployeeDepartments(int employeeId) throws Exception;

	// buscar el departamento actual
	public List<Department> getCurrentDepartments(int employeeId) throws Exception;

	// buscar a los gerentes del empleadpo
	public List<Employee> getEmployeeManagers(int employeeId) throws Exception;

	// buscar al gerente actual
	public List<Employee> getCurrentManagers(int employeeId) throws Exception;

}
