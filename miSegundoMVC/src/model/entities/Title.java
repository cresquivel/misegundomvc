package model.entities;

import java.util.Date;

public class Title {
	
	private Employee employee;
	private String title;
	private Date from_date;
	private Date to_date;
	
	public Title(){
		
	}

	public Title(Employee employee, String title, Date from_date, Date to_date) {
		super();
		this.employee = employee;
		this.title = title;
		this.from_date = from_date;
		this.to_date = to_date;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getfrom_date() {
		return from_date;
	}

	public void setfrom_date(Date from_date) {
		this.from_date = from_date;
	}

	public Date getto_date() {
		return to_date;
	}

	public void setto_date(Date to_date) {
		this.to_date = to_date;
	}
}
