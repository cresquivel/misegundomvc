package model.entities;

import java.util.Date;

public class Salary {
	private Employee employee;
	private int salary;
	private Date from_date;
	private Date to_date;
	
	public Salary(){
		
	}

	public Salary(Employee employee, int salary, Date from_date, Date to_date) {
		super();
		this.employee = employee;
		this.salary = salary;
		this.from_date = from_date;
		this.to_date = to_date;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public Date getFrom_date() {
		return from_date;
	}

	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}

	public Date getTo_date() {
		return to_date;
	}

	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}
	
	

}
