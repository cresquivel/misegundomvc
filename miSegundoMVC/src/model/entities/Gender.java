package model.entities;

public enum Gender {
	M, F;

	public static Gender getGender(String genderText) {
		if (genderText.equalsIgnoreCase("M")) {
			return Gender.M;
		} else if (genderText.equalsIgnoreCase("F")) {
			return Gender.F;
		}
		return null;
	}
}
