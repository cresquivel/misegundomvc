package model.entities;

import java.util.Date;
import java.util.List;

public class Employee {
	
	private int emp_no;
	private Date birth_date;
	private String first_name;
	private String last_name;
	private Gender gender;
	private Date hire_date;
	private List<Salary> salaries;
	private List<Title> titles;
	private List<DepartmentEmployee> deparments;
	private List<DepartmentManager> managers;
	

	public Employee(){
		
	}

	public Employee(int emp_no, Date birth_date, String first_name, String last_name, Gender gender, Date hire_date) {
		super();
		this.emp_no = emp_no;
		this.birth_date = birth_date;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.hire_date = hire_date;
	}

	public int getEmp_no() {
		return emp_no;
	}

	public void setEmp_no(int emp_no) {
		this.emp_no = emp_no;
	}

	public Date getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getHire_date() {
		return hire_date;
	}

	public void setHire_date(Date hire_date) {
		this.hire_date = hire_date;
	}
	
	public List<Salary> getSalaries() {
		return salaries;
	}

	public void setSalaries(List<Salary> salaries) {
		this.salaries = salaries;
	}

	public List<Title> getTitles() {
		return titles;
	}

	public void setTitles(List<Title> titles) {
		this.titles = titles;
	}

	public List<DepartmentEmployee> getDeparments() {
		return deparments;
	}

	public void setDeparments(List<DepartmentEmployee> deparments) {
		this.deparments = deparments;
	}

	public List<DepartmentManager> getManagers() {
		return managers;
	}

	public void setManagers(List<DepartmentManager> managers) {
		this.managers = managers;
	}


}
