package model.entities;

import java.util.Date;

public class DepartmentManager {
	private DepartamentManagerId id;
	private Date from_date;
	private Date to_date;

	public DepartmentManager() {

	}

	public DepartmentManager(DepartamentManagerId id, Date from_date, Date to_date) {

		this.id = id;
		this.from_date = from_date;
		this.to_date = to_date;
	}

	public DepartamentManagerId getId() {
		return id;
	}

	public void setId(DepartamentManagerId id) {
		this.id = id;
	}

	public Date getFrom_date() {
		return from_date;
	}

	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}

	public Date getTo_date() {
		return to_date;
	}

	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}

}
