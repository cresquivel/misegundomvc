package model.entities;

import java.util.List;

public class Department {
	
	private String dept_no;
	private String dept_name;
	private List<DepartmentEmployee> employees;
	private List<DepartmentManager> managers;
	
	public List<DepartmentEmployee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<DepartmentEmployee> employees) {
		this.employees = employees;
	}

	public List<DepartmentManager> getManagers() {
		return managers;
	}

	public void setManagers(List<DepartmentManager> managers) {
		this.managers = managers;
	}

	public Department(){
		
	}

	public Department(String dept_no, String dept_name) {
		super();
		this.dept_no = dept_no;
		this.dept_name = dept_name;
	}

	public String getDept_no() {
		return dept_no;
	}

	public void setDept_no(String dept_no) {
		this.dept_no = dept_no;
	}

	public String getDept_name() {
		return dept_name;
	}

	public void setDept_name(String dept_name) {
		this.dept_name = dept_name;
	}
	
	

}
