package model.entities;

import java.util.Date;

public class DepartmentEmployee {
	
	private DepartmentEmployeeId id;
	private Date from_date;
	private Date to_date;
	
	public DepartmentEmployee(){
		
	}
	
	public DepartmentEmployee(DepartmentEmployeeId id, Date from_date, Date to_date) {
		super();
		this.id = id;
		this.from_date = from_date;
		this.to_date = to_date;
	}


	public DepartmentEmployeeId getId() {
		return id;
	}


	public void setId(DepartmentEmployeeId id) {
		this.id = id;
	}


	public Date getFrom_date() {
		return from_date;
	}


	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}


	public Date getTo_date() {
		return to_date;
	}


	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}
	
	
}
