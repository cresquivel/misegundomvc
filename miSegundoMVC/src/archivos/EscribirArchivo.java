package archivos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class EscribirArchivo {

	public static void main(String[] args) {
		File f;
		f = new File("output.txt");
		// Escritura
		try {
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(w);
			PrintWriter wr = new PrintWriter(bw);
			wr.write("Esta es una linea de codigo");// escribimos en el archivo
			wr.write(" - y aqui continua"); // concatenamos en el archivo sin
												// borrar lo existente
			// ahora cerramos los flujos de canales de datos, al cerrarlos el
			// archivo quedar� guardado con informaci�n escrita
			// de no hacerlo no se escribir� nada en el archivo
			wr.close();
			bw.close();
		} catch (IOException e) {

		}
		System.out.println("Fin de programa");
	}
}
