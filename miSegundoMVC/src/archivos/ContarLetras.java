package archivos;

import java.io.FileReader;
import java.io.IOException;

public class ContarLetras {

	public static void main(String args[]) {
		try {
			String FILE_NAME = "input.txt";
			String VOCALS = "aeiou";
			// CharCounter counters = new CharCounter(VOCALS, false);
			FileReader input = new FileReader(FILE_NAME);
			int contarA=0;
			int contarE=0;
			int contarI=0;
			int contarO=0;
			int contarU=0;
			
			int c = input.read();
			int totalLetras=0;
			while (c != -1) {
				System.out.println(c);
				// counters.countIfTargeted((char)c);
				c = input.read();
				char letra = (char)c;
				if(letra=='a'||letra=='A') contarA++;
				else if (letra=='e'||letra=='E') contarE++;
				else if (letra=='i'||letra=='I'||letra=='�') contarI++;
				else if (letra=='o'||letra=='O') contarO++;
				else if (letra=='u'||letra=='U') contarU++;
				
			}
			input.close();
			System.out.println("Hubo un total "+contarA+ " letras A");
			System.out.println("Hubo un total "+contarE+ " letras E");
			System.out.println("Hubo un total "+contarI+ " letras I");
			System.out.println("Hubo un total "+contarO+ " letras O");
			System.out.println("Hubo un total "+contarU+ " letras U");
			// println(counters.toString());
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("Something bad has happended");
		}
	}

}
